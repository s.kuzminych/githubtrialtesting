# GithubTrialTesting
### Selenium IDE Task: 
- [Selenium IDE Project](https://gitlab.com/s.kuzminych/githubtrialtesting/tree/master/GithubSearchIDETests)
***
### Selenium Grid Task (one local machine version: Chrome + Firefox)
- [Tests](https://gitlab.com/s.kuzminych/githubtrialtesting/blob/master/GithubOpeningGridTests/GithubOpeningGridTests/GithubOperningGridTests.cs)

##### Preset: #####
1. Download drivers to use WebDrivers API for browsers:
- [Geckodriver for Firefox](https://github.com/mozilla/geckodriver/releases) | [Chromedriver for Chrome](https://chromedriver.chromium.org/)
2. Download Selenium Server (.jar file) (and install Java, if you dont have it already):
- [Selenium Server](https://selenium.dev/downloads/)
3. Open a command prompt and navigate to the directory where the `selenium-server-standalone.jar` file is saved. Start the Hub:

`java -jar selenium-server-standalone-%VERSION%.jar -role hub`

4. Open another command prompt in the same directory. Start Chrome Node:

`java -Dwebdriver.chrome.driver="%PATH_TO_CHROMEDRIVER%\chromedriver.exe" -jar selenium-server-standalone-%VERSION%.jar -role node -hub http://localhost:4444/grid/register/`

5. Open another command prompt in the same directory. Start Firefox Node:

`java -Dwebdriver.firefox.driver="%PATH_TO_GECKODRIVER%\geckodriver.exe" -jar selenium-server-standalone-%VERSION%.jar -role node -hub http://localhost:4444/grid/register/`

6. Run the tests from Visual Studio Test Explorer.

