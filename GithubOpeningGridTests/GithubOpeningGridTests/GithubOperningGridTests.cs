using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace GithubOpeningGridTests
{
    public class Tests
    {
        IWebDriver driver;
        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
        [Test]
        public void OpeningFirefoxTest()
        {
            FirefoxOptions options = new FirefoxOptions();
            driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), options);
            driver.Navigate().GoToUrl("https://github.com/");
            Assert.AreEqual("The world�s leading software development platform � GitHub", driver.Title);
        }
        [Test]
        public void OpeningChromeTest()
        {
            ChromeOptions options = new ChromeOptions();
            driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), options);
            driver.Navigate().GoToUrl("https://github.com/");
            Assert.AreEqual("The world�s leading software development platform � GitHub", driver.Title);
        }

    }
}